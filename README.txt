## What is the purpose of this repository ?

This repo is used to create the "grub-efi-arm64-netdir" package.
It installs grub efi binary for arm64 in /var/lib/tftpboot/grub so that a DHCP servers (x86 or arm64) can serve grub over PXE boot to arm64 clients.

It is used by kadeploy in Grid'5000 for arm64 nodes.


## How is the package build ?

gitlab-ci is used to reserve a arm64 node on Grid'5000 and build the package on it.
This repository contains only meta informations about the package (debian directory) and the version file containing only the version of the package to be build.
The actual grub efi arm64 binary that is present in the final package is generated on the arm64 node using the command "grub-mknetdir"
